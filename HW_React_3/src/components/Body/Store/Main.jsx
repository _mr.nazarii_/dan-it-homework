import React, { useEffect, useState } from "react";
import FlexElement from "../../Reusable/FlexElement";
import Item from "./Item";

const Main = (props) => {
  const [cookies, setCookies] = useState(null);

  useEffect(() => {
    fetch("http://localhost:8000/itemsCards")
      .then((res) => res.json())
      .then((data) => setCookies(data));
  }, []);

  const outputInfo = () => {
    if (cookies) {
      let num = 0;
      return (
        <>
          {cookies.map(
            ({ id, name, price, itemCode, color, url, starColor }) => {
              num++;
              const time = new Date().getTime().toString() + num;

              return (
                <Item
                  color={color}
                  id={id}
                  name={name}
                  price={price}
                  itemCode={itemCode}
                  key={time}
                  url={url}
                  starColor={starColor}
                  //
                  rateFunc={props.rateFunc}
                  closeFunc={props.closeFunc}
                />
              );
            }
          )}
        </>
      );
    } else {
      return null;
    }
  };

  return (
    <>
      <FlexElement justify={"center"} color={"#000"} radius={"0px"}>
        {outputInfo()}
      </FlexElement>
    </>
  );
};

export default Main;
