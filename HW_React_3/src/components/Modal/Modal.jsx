import React from "react";

import PropTypes from "prop-types";

import "./Modal.style.scss";

import Button from "../Button/Button";

import FlexElement from "../Reusable/FlexElement";

const Modal = (props) => {
  if (props.show === false) {
    return null;
  } else if (props.show === true) {
    return (
      <>
        <div onClick={props.closeModal} className={"modalBackground"}></div>

        <div className="modal">
          <FlexElement
            elementColor={"#ffffff"}
            justify={"space-between"}
            bgColor={props.headerModal}
            radius={"5px 5px 0px 0px"}
          >
            <h2>{props.header}</h2>
            {!props.closeButton ? (
              <Button
                backgroundColor={"rgba(33, 122, 60, 0)"}
                width={"35px"}
                height={"35px"}
                onClick={props.closeModal}
                closeButton={props.closeButton}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="25"
                  height="25"
                  fill="white"
                  viewBox="0 0 16 16"
                >
                  <path
                    fill="evenodd"
                    d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"
                  />
                  <path
                    fill="evenodd"
                    d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"
                  />
                </svg>
              </Button>
            ) : null}
          </FlexElement>
          <FlexElement
            elementColor={"#ffffff"}
            justify={"center"}
            bgColor={props.bodyModal}
            radius={"0px"}
          >
            <p>{props.children}</p>
          </FlexElement>
          <FlexElement
            elementColor={"#ffffff"}
            justify="center"
            radius={"0px 0px 5px 5px"}
            bgColor={props.bodyModal}
          >
            {props.actions
              ? props.actions.map((element) => {
                  return element;
                })
              : null}
          </FlexElement>
        </div>
      </>
    );
  }
};

Modal.propTypes = {
  actions: PropTypes.array,
};

Modal.defaultProps = {
  actions: [],
};

export default Modal;
