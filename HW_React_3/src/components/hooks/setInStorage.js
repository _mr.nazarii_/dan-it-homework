const setInStorage = (id, element, action) => {
  const array = JSON.parse(localStorage.getItem(element));

  if (action === "set") {
    if (array) {
      if (array.includes(id)) {
        return;
      } else {
        array.push(id);
        localStorage.setItem(element, JSON.stringify(array));
      }
    } else if (!array) {
      localStorage.setItem(element, JSON.stringify([id]));
    }
  } else if (action === "remove") {
    if (array.length > 1) {
      const newArr = array.filter((elem) => elem !== id);
      localStorage.setItem(element, JSON.stringify(newArr));
    } else {
      localStorage.removeItem(element);
    }
  }
};

export default setInStorage;
