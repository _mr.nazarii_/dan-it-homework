import "./App.scss";
import React, { useState } from "react";
import Modal from "./components/Modal/Modal";
import Main from "./components/Body/Store/Main";
import Header from "./components/Header/Header.jsx";
import { Routes, Route } from "react-router-dom";
import Cart from "./components/Body/Cart/Cart";
import Favorite from "./components/Body/Favorite/Favorite";
import { click } from "@testing-library/user-event/dist/click";

const App = () => {
  const [modal, setModal] = useState(false);
  const [modalText, setModalText] = useState("");
  const [modalTitle, setModalTitle] = useState("");
  const [headerModal, setHeaderModal] = useState("");
  const [bodyModal, setBodyModal] = useState("");
  const [closeButton, setCloseButton] = useState(false);
  const [actions, setActions] = useState(null);

  const objectReturn = (
    modal,
    modalTitle,
    modalText = "Default",
    headerModal,
    bodyModal,
    closeButton,
    actions
  ) => {
    const clickHandler = (e) => {
      setModal(modal);
      setModalTitle(modalTitle);
      setModalText(modalText);
      setHeaderModal(headerModal);
      setBodyModal(bodyModal);
      setCloseButton(closeButton);
      setActions(actions);
    };

    return clickHandler;
  };

  const closeModal = (e) => {
    setModal(false);
  };

  return (
    <>
      <Routes>
        <Route path={"/"} element={<Header />}>
          <Route
            path={"cart"}
            element={<Cart rateFunc={objectReturn} closeFunc={closeModal} />}
          />

          <Route
            index
            element={<Main rateFunc={objectReturn} closeFunc={closeModal} />}
          />
          <Route
            path={"favorite"}
            element={
              <Favorite rateFunc={objectReturn} closeFunc={closeModal} />
            }
          />
        </Route>
      </Routes>

      <Modal
        header={modalTitle}
        show={modal}
        closeModal={closeModal}
        headerModal={headerModal}
        closeButton={closeButton}
        bodyModal={bodyModal}
        actions={actions}
      >
        {modalText}
      </Modal>
    </>
  );
};

App.defaultProps = {
  modalText: "Default",
  btnTitle: "Button",
  actions: [],
};

export default App;
