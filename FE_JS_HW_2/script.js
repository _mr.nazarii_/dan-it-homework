const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];
let num = 0;
const rootEl = document.getElementById("root");
const listEl = document.createElement("ul");
rootEl.appendChild(listEl);

function list(arr) {
  for (const i of arr) {
    if (
      Object.hasOwn(i, "author") &&
      Object.hasOwn(i, "name") &&
      Object.hasOwn(i, "price")
    ) {
      const titleEl = document.createElement("h1");
      num++;
      titleEl.textContent = `Book: ${num}`;

      listEl.appendChild(titleEl);
    } else {
    }

    for (const [k, v] of Object.entries(i)) {
      const listItemEl = document.createElement("li");
      try {
        if (
          Object.hasOwn(i, "author") &&
          Object.hasOwn(i, "name") &&
          Object.hasOwn(i, "price")
        ) {
          listItemEl.textContent = k + ": " + v;
          listEl.appendChild(listItemEl);
        } else {
          if (!Object.hasOwn(i, "author")) {
            throw new Error("No author specified");
          }
          if (!Object.hasOwn(i, "name")) {
            throw new Error("No name specified");
          }
          if (!Object.hasOwn(i, "price")) {
            throw new Error("No price specified");
          }
        }
      } catch (e) {
        console.log(e.stack);
      }
    }
  }
}

list(books);
