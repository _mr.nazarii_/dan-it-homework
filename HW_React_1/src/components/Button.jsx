import React from "react";

import { ButtonStyle } from "./Styles.jsx";
import Modal from "./Modal";

class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      modalTitle: this.props.modalTitle,
      modalText: this.props.modalText,
      headerModal: this.props.headerModal,
      bodyModal: this.props.bodyModal,
      buttonsModal: this.props.buttonsModal,
      closeButton: this.props.closeButton,
    };
  }

  clickHandler = () => {
    return this.setState({
      show: true,
    });
  };

  closeModal = (e) => {
    return this.setState({ show: false });
  };

  render() {
    return (
      <>
        <ButtonStyle
          backgroundColor={this.props.backgroundColor}
          borderColor={this.props.borderColor}
          width={this.props.width}
          height={this.props.height}
          onClick={this.clickHandler}
        >
          {this.props.text}
        </ButtonStyle>
        {this.props.secondButton ? (
          <Modal
            header={this.state.modalTitle}
            show={this.state.show}
            closeModal={this.closeModal}
            headerModal={this.state.headerModal}
            bodyModal={this.state.bodyModal}
            closeButton={this.props.closeButton}
            actions={[
              <ButtonStyle
                backgroundColor={this.props.buttonsModal}
                width={"101px"}
                height={"41px"}
                onClick={this.closeModal}
              >
                {this.props.firstButton}
              </ButtonStyle>,
              <ButtonStyle
                backgroundColor={this.props.buttonsModal}
                width={"101px"}
                height={"41px"}
                onClick={this.closeModal}
              >
                {this.props.secondButton}
              </ButtonStyle>,
            ]}
          >
            {this.state.modalText}
          </Modal>
        ) : (
          <Modal
            header={this.state.modalTitle}
            show={this.state.show}
            closeModal={this.closeModal}
            headerModal={this.state.headerModal}
            closeButton={this.props.closeButton}
            bodyModal={this.state.bodyModal}
            actions={[
              <ButtonStyle
                backgroundColor={this.props.buttonsModal}
                width={"101px"}
                height={"41px"}
                onClick={this.closeModal}
              >
                {this.props.firstButton}
              </ButtonStyle>,
            ]}
          >
            {this.state.modalText}
          </Modal>
        )}
      </>
    );
  }
}

export default Button;
