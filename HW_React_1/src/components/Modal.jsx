import React from "react";
import {
  ButtonStyle,
  ModalStyle,
  FlexElement,
  ModalBackground,
} from "./Styles.jsx";

class Modal extends React.Component {
  render() {
    if (this.props.show === false) {
      return null;
    } else if (this.props.show === true) {
      return (
        <>
          <ModalBackground onClick={this.props.closeModal}></ModalBackground>

          <ModalStyle>
            <FlexElement justify="space-between" color={this.props.headerModal}>
              <h2>{this.props.header}</h2>
              <ButtonStyle
                backgroundColor={"rgba(33, 122, 60, 0)"}
                width={"35px"}
                height={"35px"}
                onClick={this.props.closeModal}
                closeButton={this.props.closeButton}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="25"
                  height="25"
                  fill="white"
                  class="bi bi-x-lg"
                  viewBox="0 0 16 16"
                >
                  <path
                    fill-rule="evenodd"
                    d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"
                  />
                  <path
                    fill-rule="evenodd"
                    d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"
                  />
                </svg>
              </ButtonStyle>
            </FlexElement>
            <FlexElement radius={"0px"} color={this.props.bodyModal}>
              <p>{this.props.children}</p>
            </FlexElement>
            <FlexElement
              justify="center"
              radius={"0px 0px 5px 5px"}
              color={this.props.bodyModal}
            >
              {this.props.actions.map((element) => {
                return element;
              })}
            </FlexElement>
          </ModalStyle>
        </>
      );
    }
  }
}

export default Modal;
