import styled from "styled-components";

const ButtonStyle = styled.button`
  background-color: ${(props) =>
    props.backgroundColor ? props.backgroundColor : "none"};

  width: ${(props) => (props.width ? props.width : "500px")};
  height: ${(props) => (props.height ? props.height : "500px")};
  border: none;
  border-radius: 5px;
  margin: 10px;
  display: ${(props) => (props.closeButton === true ? "none" : "inline-block")};
  font-size: 14px;
  font-weight: normal;

  color: ${(props) => (props.borderColor ? props.borderColor : "white")};

  svg {
    position: relative;
    top: 0px;
    left: -1px;
  }
`;

const ModalStyle = styled.div`
  border-radius: 17px;
  border: none;
  width: 500px;
  position: fixed;
  /* padding: 10px 40px; */
  top: 30%;
  transform: translateX(-50%);
  left: 50%;
  text-align: center;
  line-height: 30px;
`;

const ModalBackground = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #383838ab;
  position: fixed;
  top: 0;
  left: 0;
`;

const FlexElement = styled.div`
  display: flex;
  justify-content: ${(props) => (props.justify ? props.justify : "flex-start")};
  align-items: center;
  background: ${(props) => (props.color ? props.color : "#e74c3c")};
  border-radius: ${(props) =>
    props.radius ? props.radius : "5px 5px 0px 0px"};
  color: white;
  padding: 20px 30px 16px 30px;

  h2 {
    margin: 0px;
    font-size: 1.3em;
  }
  p {
    margin: 0;
    font-size: 14px;
  }
  button {
    margin: 0 6px;
  }
`;

export { ButtonStyle, ModalStyle, FlexElement, ModalBackground };
