const menuBtn = document.querySelector(".menu-btn");
const content = document.getElementById("nav");
let menuOpen = false;
menuBtn.addEventListener("click", () => {
  if (!menuOpen) {
    menuBtn.classList.add("open");
    content.style.cssText = "display: inline-block";

    menuOpen = true;
  } else {
    menuBtn.classList.remove("open");
    content.style.removeProperty("display");

    menuOpen = false;
  }
});
