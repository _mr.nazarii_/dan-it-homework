"use strict";

let active = null;

const button = [...document.getElementsByClassName("btn")];

document.addEventListener("keydown", (e) => {
  let target = e.key;

  button.forEach((element) => {
    if (element.textContent === target.toUpperCase()) {
      highlight(element);
    } else if (element.textContent === target) {
      highlight(element);
    } else {
      return;
    }
  });
});

function highlight(element) {
  if (!active) {
    active = element;
    active.classList.add("special");
  } else if (active) active.classList.remove("special");
  active = element;
  active.classList.add("special");
}
