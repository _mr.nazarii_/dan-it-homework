import "./App.scss";
import React from "react";

import Modal from "./components/Modal/Modal";

import Main from "./components/Body/Main.jsx";

import Header from "./components/Header/Header.jsx";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      header: "",
      modalTitle: "",
      modalText: "",
      headerModal: "",
      bodyModal: "",
      buttonsModal: "",
      closeButton: false,
      firstButton: "",
      secondButton: "",
      actions: null,
      cart: false,
      favorite: false,
      main: true,
    };
  }

  objectReturn = (
    showModal,
    modalTitle,
    modalText,
    headerModal,
    bodyModal,
    closeButton,
    actions = [],
    btnTitle = "Button"
  ) => {
    const object = {
      showModal: showModal,
      modalTitle: modalTitle,
      modalText: modalText,
      headerModal: headerModal,
      bodyModal: bodyModal,
      closeButton: closeButton,
      actions: actions,
      btnTitle: btnTitle,
    };

    const clickHandler = (e) => {
      return this.setState(object);
    };

    return clickHandler;
  };

  switcherState = (e) => {
    let target = e.target;

    if (target.className.includes("cart")) {
      this.setState({ cart: true, favorite: false, main: false });
    } else if (target.className.includes("favorite")) {
      this.setState({ cart: false, favorite: true, main: false });
    } else if (target.className.includes("main")) {
      this.setState({ cart: false, favorite: false, main: true });
    }
  };

  closeModal = (e) => {
    return this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        <Header switcherState={this.switcherState} />
        <Main
          rateFunc={this.objectReturn}
          closeFunc={this.closeModal}
          cart={this.state.cart}
          favorite={this.state.favorite}
          main={this.state.main}
        ></Main>
        <Modal
          header={this.state.modalTitle}
          show={this.state.showModal}
          closeModal={this.closeModal}
          headerModal={this.state.headerModal}
          closeButton={this.state.closeButton}
          bodyModal={this.state.bodyModal}
          actions={this.state.actions}
        >
          {this.state.modalText}
        </Modal>
      </>
    );
  }
}

export default App;
