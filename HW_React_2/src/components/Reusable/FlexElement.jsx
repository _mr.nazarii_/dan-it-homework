import React from "react";

class FlexElement extends React.Component {
  render() {
    const wrapper = {
      justifyContent: this.props.justify,
      background: this.props.bgColor,
      borderRadius: this.props.radius,
      color: this.props.elementColor,
    };
    return (
      <>
        <div className="wrapper" style={wrapper}>
          {this.props.children}
        </div>
      </>
    );
  }
}

export default FlexElement;
