import React from "react";
import FlexElement from "../Reusable/FlexElement";
import Item from "./Item";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      favArray: null,
      cartArray: null,
    };
  }

  setFavorite = (element, where) => {
    if (where === "fav") {
      if (this.state.favArray === null) {
        localStorage.setItem("favArray", [element]);

        this.setState({ favArray: [element] });
      } else {
        const newArr = [localStorage.getItem("favArray")];

        newArr.push(element);
        localStorage.setItem("favArray", newArr);
        this.setState({ favArray: [...this.state.favArray, element] });
      }
    } else if (where === "cart") {
      if (this.state.cartArray === null) {
        localStorage.setItem("cartArray", [element]);

        this.setState({ cartArray: [element] });
      } else {
        const newArr = [localStorage.getItem("cartArray")];

        newArr.push(element);

        localStorage.setItem("cartArray", newArr);
        this.setState({ cartArray: [...this.state.cartArray, element] });
      }
    }
  };

  componentDidMount() {
    fetch("http://localhost:8000/itemsCards")
      .then((res) => res.json())
      .then((data) => this.setState({ cookies: data }));

    const fav = localStorage.getItem("favArray");
    const cart = localStorage.getItem("cartArray");

    this.setState({ favArray: fav, cartArray: cart });
  }

  outputInfo() {
    if (this.state.cookies) {
      let num = 0;
      return (
        <>
          {this.state.cookies.map(
            ({ id, name, price, itemCode, color, url, starColor }) => {
              num++;
              const time = new Date().getTime().toString() + num;

              return (
                <Item
                  color={color}
                  id={id}
                  name={name}
                  price={price}
                  itemCode={itemCode}
                  key={time}
                  url={url}
                  starColor={starColor}
                  //
                  rateFunc={this.props.rateFunc}
                  closeFunc={this.props.closeFunc}
                  favoriteFunc={this.setFavorite}
                />
              );
            }
          )}
        </>
      );
    } else {
      return null;
    }
  }

  favoriteOutput() {
    if (this.state.favArray) {
      let num = 20;
      return (
        <>
          {this.state.cookies.map(
            ({ id, name, price, itemCode, color, url }) => {
              num++;
              const time = new Date().getTime().toString() + num;

              if (this.state.favArray.includes(id)) {
                return (
                  <Item
                    color={color}
                    id={id}
                    name={name}
                    price={price}
                    itemCode={itemCode}
                    key={time}
                    url={url}
                    //
                    rateFunc={this.props.rateFunc}
                    closeFunc={this.props.closeFunc}
                    favoriteFunc={this.setFavorite}
                    starColor={true}
                  />
                );
              } else {
                return null;
              }
            }
          )}
        </>
      );
    } else if (this.state.favArray === null) {
      return <h1 className="emptyCart">Favorite list is empty</h1>;
    }
  }

  cartOutput() {
    if (this.state.cartArray) {
      let num = 10;
      return (
        <>
          {this.state.cookies.map(
            ({ id, name, price, itemCode, color, url, starColor }) => {
              num++;
              const time = new Date().getTime().toString() + num;

              if (this.state.cartArray.includes(id)) {
                return (
                  <Item
                    color={color}
                    id={id}
                    name={name}
                    price={price}
                    itemCode={itemCode}
                    key={time}
                    url={url}
                    starColor={starColor}
                    //
                    rateFunc={this.props.rateFunc}
                    closeFunc={this.props.closeFunc}
                    favoriteFunc={this.setFavorite}
                  />
                );
              } else {
                return null;
              }
            }
          )}
        </>
      );
    } else if (this.state.cartArray === null) {
      return <h1 className="emptyCart">Cart is empty</h1>;
    }
  }

  switcher() {
    if (this.props.main) {
      return this.outputInfo();
    } else if (this.props.cart) {
      if (this.state.cartArray) {
        return this.cartOutput();
      } else {
        return <h1 className="emptyCart">Cart is empty</h1>;
      }
    } else if (this.props.favorite) {
      if (this.state.favArray) {
        return this.favoriteOutput();
      } else {
        return <h1 className="emptyCart">Favorite list is empty</h1>;
      }
    }
  }

  render() {
    return (
      <>
        <FlexElement justify={"center"} color={"#000"} radius={"0px"}>
          {this.switcher()}
        </FlexElement>
      </>
    );
  }
}

export default Main;
