import React from "react";
import Button from "./Button";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.append(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with inner context or without", async () => {
  let promise = Promise.resolve();

  render(<Button>Hello</Button>, container);
  expect(container.textContent).toBe("Hello");

  render(<Button></Button>, container);
  expect(container.textContent).toBe("Button");

  await act(() => promise);
});
