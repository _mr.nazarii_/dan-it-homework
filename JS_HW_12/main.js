"use strict";

const images = document.getElementById("images-wrapper");

const imageToShow = [...document.getElementsByClassName("image-to-show")];

const stopBtn = document.createElement("button");
stopBtn.textContent = "Stop";
stopBtn.style.position = "absolute";
stopBtn.style.top = "420px";
stopBtn.style.left = "120px";

const continueBtn = document.createElement("button");
continueBtn.textContent = "Continue";
continueBtn.style.position = "absolute";
continueBtn.style.top = "420px";
continueBtn.style.left = "180px";

window.addEventListener("load", () => {
  document.body.append(stopBtn, continueBtn);
});

imageToShow.forEach((e) => {
  e.style.position = "absolute";
});

let num = 0;

let count = 1;

let time;

function repeat() {
  if (num === 4) num = 0;

  imageToShow[num].style.zIndex = count;

  count++;
  num++;

  time = setTimeout(repeat, 3000);
}

repeat();

document.addEventListener("click", (e) => {
  let target = e.target;

  if (target.textContent === "Stop") {
    clearTimeout(time);
    target.setAttribute("disabled", true);
    continueBtn.removeAttribute("disabled", false);
  } else if (target.textContent === "Continue") {
    target.setAttribute("disabled", true);
    stopBtn.removeAttribute("disabled");
    repeat();
  }
});
