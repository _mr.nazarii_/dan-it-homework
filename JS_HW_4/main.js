"use strict";

// Main Assigment

const newUser = {
  createNewUser() {
    // Create user function
    let q1 = prompt("What is you name?");
    let q2 = prompt("What is you surname?");

    return Object.defineProperties(newUser, {
      fName: { value: q1, writable: true },
      lName: { value: q2, writable: true },
    });
  },

  getLogin() {
    // Login function
    let info = `${this.fName.charAt(0)}.${this.lName}`;
    info = info.toLowerCase();
    return info;
  },

  set fName(val) {
    this.fName = val;
  },

  get fName() {
    return this.fName;
  },

  set lName(val) {
    this.lName = val;
  },
  get lName() {
    return this.lName;
  },
};

newUser.createNewUser();
console.log(newUser.getLogin());

console.log(newUser);

console.log((newUser.fName = "Waldo"));
console.log((newUser.lName = "Surnamdo"));

console.log(newUser);
console.log(newUser.getLogin());
