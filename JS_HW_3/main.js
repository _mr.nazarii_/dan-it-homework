"use strict";

// Main Assigment

function countNum(a, b, operator = "+") {
  if (operator == "+") {
    return a + b;
  } else if (operator == "-") {
    return a - b;
  } else if (operator == "*") {
    return a * b;
  } else if (operator == "/") {
    return a / b;
  }
}

while (true) {
  let num1 = +prompt("Your first number", 2);
  let num2 = +prompt("Your second number", 2);
  let operator = prompt("Your operator", "+");

  if (
    isNaN(+num1) ||
    isNaN(+num2) ||
    typeof +num1 != "number" ||
    typeof +num2 != "number" ||
    !operator
  ) {
    alert("Not correct input");
    continue;
  }

  console.log(countNum(num1, num2, operator));
  break;
}
// Additional Assigment
