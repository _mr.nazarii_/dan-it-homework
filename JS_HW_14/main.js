"use strict";

$(document).ready(function () {
  $("#toTop").hide();

  $(".item-links,.fixed-button").on("click", function (e) {
    e.preventDefault();

    let link = $(this).attr("href");
    $("html, body").animate(
      {
        scrollTop: $(link).offset().top,
      },
      800
    );
  });

  $(window).on("scroll", function () {
    // scroll cordinates of the page from 0 to vertical end
    let top = $(this).scrollTop();
    let heightOfScreen = $(this).height();
    // if less then the point it fades In
    if (top < heightOfScreen) {
      $("#toTop").fadeOut();
      // if more then the point it fades Out
    } else if (top > heightOfScreen) {
      $("#toTop").fadeIn();
    }
  });

  $("#hide").on("click", function (e) {
    $(".page-section-1").slideToggle(1000);
  });
});
