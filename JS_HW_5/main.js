"use strict";

// Main Assigment

const newUser = {
  getLogin() {
    // Login function
    let info = `${this.fName.charAt(0)}.${this.lName}`;
    info = info.toLowerCase();
    return info;
  },

  setFirstName(val) {
    this.fName = val;
  },
  setLastName(val) {
    this.lName = val;
  },
  // this.age
  getAge() {
    let now = Date.now();
    let birth = new Date(this.birthDate);
    birth = birth.getTime();

    let dif = ((now - birth) / 1000) * 60 * 60 * 24 * 364.3;
    return dif.toString().slice(0, 2) + " years old";
  },

  getPassword() {
    let info = `${this.fName
      .charAt(0)
      .toUpperCase(0)}${this.lName.toLowerCase()}${this.birthDate.slice(0, 4)}`;

    return info;
  },
};

function createNewUser() {
  // Create user function
  debugger;
  let q1 = prompt("What is your Name");
  let q2 = prompt("What is your Surname");
  let q3 = prompt(
    "When is your birthday? Please input in the following format (dd-mm-yyyy)"
  );

  // Change format of date input (yyyy-mm-dd)
  q3 = q3.split("-");
  q3 = [q3[2], q3[1], q3[0]].join("-");

  return Object.defineProperties(newUser, {
    fName: { value: q1, writable: true },
    lName: { value: q2, writable: true },
    birthDate: { writable: true, value: q3 },
  });
}

createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
