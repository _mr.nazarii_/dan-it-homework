"use strict";

const arr1 = ["1", "2", "3", "sea", "user", 23];

function listSetter(arr, root = document.body) {
  const ul1 = document.createElement("ul");
  const li2 = document.createElement("li");
  arr.map((x) => {
    if (Array.isArray(x)) {
    } else {
      const li1 = document.createElement("li");
      li1.textContent = x;
      ul1.append(li1);
      root.append(ul1);
    }
  });
}

listSetter(arr1);
//
function createUl(arr) {
  const ul = document.createElement("ul");
  const li = document.createElement("li");
  arr.forEach((item) => {
    const newLi = li.cloneNode(true);
    newLi.textContent = item;
    ul.appendChild(newLi);
  });

  return ul;
}
