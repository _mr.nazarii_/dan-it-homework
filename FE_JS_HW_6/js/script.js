const url = "https://api.ipify.org/?format=json";

const fullurl = "http://ip-api.com/json/";

const field = "?fields=1634303";

const btn = document.getElementById("button");
const col = document.getElementById("col");

btn.addEventListener("click", () => {
  async function getIP(url, fullurl, field) {
    const response = await fetch(url);
    const ipAdress = await response.json();
    console.log(ipAdress.ip);

    const ipInfoResponse = await fetch(fullurl + ipAdress.ip + field);
    const resp = await ipInfoResponse.json();
    console.log(resp);

    const { continent, country, region, city, district } = resp;

    createElement(continent, col);
    createElement(country, col);
    createElement(region, col);
    createElement(city, col);
    createElement(district, col);
  }

  function createElement(content, col) {
    const h3 = document.createElement("h3");
    h3.innerHTML = content;
    col.append(h3);
  }

  getIP(url, fullurl, field);
});
