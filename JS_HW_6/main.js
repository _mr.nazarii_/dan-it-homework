"use strict";

const arr = ["hello", "world", 23, "23", null];
function filterBy(arr, datatype) {
  return arr.filter((element) => typeof element !== datatype);
}

console.log(filterBy(arr, "object"));
