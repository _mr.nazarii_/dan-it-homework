// Task 1

const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

function mergeTheCompny(a, b) {
  const newArr = [...a, ...b];
  const unique = Array.from(new Set(newArr));
  return unique;
}

console.log(mergeTheCompny(clients1, clients2));

// Task 2

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

function loopInside(data) {
  const charactersShortInfo = [];

  for (const iterator of data) {
    const { gender, status, ...other } = iterator;
    charactersShortInfo.push(other);
  }
  console.log(charactersShortInfo);
}

loopInside(characters);

// Task 3

let user1 = {
  name: "John",
  years: 30,
};

let { name, years, isAdmin = false } = user1;

console.log(`name: ${name} age: ${years} adminStatus: ${isAdmin}`);

// Task 4

const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const object = {
  ...satoshi2018,
  ...satoshi2019,
  ...satoshi2020,
};

console.log(object);

// Task 5

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

let newArr = [];

function addBook(obj, add) {
  for (const i of obj) {
    newArr.push(i);
  }
  newArr.push(add);
  console.log(newArr);
}
addBook(books, bookToAdd);

// Task 6

const employee = {
  firstname: "Vitalii",
  lastname: "Klichko",
};

let { firstname, lastname } = employee;

const employee2 = { ...employee, age: 20, salary: "120000$" };

console.log(employee2);

// Task 7

const array = ["value", () => "showValue"];

const value = array.slice(0, 1);
value.join("");
function showValue() {
  let i = array.slice(1, 2).join("").slice(7, -1);
  return i;
}

alert(value); // должно быть выведено 'value'
alert(showValue()); // должно быть выведено 'showValue'
