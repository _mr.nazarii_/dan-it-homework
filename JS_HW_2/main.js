"use strict";

// Main Assigment

while (true) {
  // Start of the loop
  let inp = +prompt("Enter number");

  // Input filter
  if (isNaN(+inp) || typeof +inp !== "number" || !inp || inp < 0) {
    alert("It is not a whole Number, please repeat");
    continue;
  }

  // Caclculations
  if (inp % 5 !== 0) {
    alert("Sorry, no numbers");
    break;
  }

  for (let i = 0; i <= inp; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
  break;
}

// Additional Assigment
while (true) {
  let m = +prompt("Enter first number");
  let n = +prompt("Enter second number");

  // Input filter
  if (
    isNaN(+m) ||
    isNaN(+n) ||
    typeof +m !== "number" ||
    typeof +n !== "number" ||
    !m ||
    !n ||
    m < 0 ||
    n < 0
  ) {
    alert("It is not a whole Number, please repeat");
    continue;
  }
  // Prime number
  prime: for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {
      if (i % 2 == 0) {
        continue prime;
      }
    }
    console.log(i);
  }
  break;
}
