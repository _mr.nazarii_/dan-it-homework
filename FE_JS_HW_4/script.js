const url = "https://ajax.test-danit.com/api/swapi/films";

const root = document.getElementById("root");
// const middle = document.getElementsByClassName("middle")[0];

console.log(document.readyState);

class MovieInfo {
  constructor(url, root) {
    this.url = url;
    this.root = root;
  }

  _addBr() {
    const br1 = document.createElement("br");
    return br1;
  }

  _loader() {
    return `<div class="middle">
      <div class="bar bar1"></div>
      <div class="bar bar2"></div>
      <div class="bar bar3"></div>
      <div class="bar bar4"></div>
      <div class="bar bar5"></div>
      <div class="bar bar6"></div>
      <div class="bar bar7"></div>
      <div class="bar bar8"></div>
    </div>`;
  }

  getInfo() {
    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        return response.forEach((arr) => {
          root.append(this._addBr());

          const li = document.createElement("li");
          li.style.fontSize = "30px";
          li.style.fontWeight = "800";

          li.append(`Episode: ${arr.id} Title: ${arr.name}`);
          root.appendChild(li);

          root.append(this._addBr());

          const cap = document.createElement("li");
          const hero = document.createElement("ul");
          hero.style.display = "none";

          cap.style.width = "830px";

          cap.append(arr.openingCrawl);
          cap.append(this._addBr());

          arr.characters.forEach((e) => {
            fetch(e)
              .then((response) => {
                return response.json();
              })
              .then((response) => {
                let { name } = response;
                const cap = document.createElement("li");

                cap.textContent = name;

                hero.append(cap);

                hero.append(this._addBr());
              });
          });

          root.appendChild(cap);

          root.appendChild(hero);

          const div = document.createElement("div");
          root.append(div);
          div.innerHTML = this._loader();
          div.style.margin = "80px 0px 20px 0px";
          hero.style.margin = "40px 0px 20px 0px";

          setTimeout(() => {
            div.style.display = "none";
            hero.style.display = "block";
          }, 3000);
        });
      });

    console.log(document.readyState);
  }
}

const info = new MovieInfo(url, root);

info.getInfo();
